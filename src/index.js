import React from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router';
import { createHashHistory } from "history";


class Rights extends React.Component {


  constructor(props) {

    super(props);

    this.state ={
      acces: null,
      text: '',
    }

    this.handleChange = this.handleChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(str) {
    this.setState({text: str.target.value});
  }

  handleSubmit(str) {

    str.preventDefault();

    this.setState({acces: this.state.text});
  }

  AddBoard() {

    return (<Board rightAccess={this.state.acces} />);
  }

  render() {
    if (this.state.acces)
    {
      return (
        <div>
          tu as les droits de: {this.state.acces}

          {this.AddBoard()}  
        </div>
      );
    }
    else
    {
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
            <label>
              quels sont tes droits ?
            </label>
            <input onChange={this.handleChange} value={this.state.text}/>
            <button>
              validez vos droits
            </button>
          </form>
        </div>
      );
    }
  }
}

class Board extends React.Component {

  constructor(props) {

    super(props);

    this.access = props.rightAccess;

    this.AllApps = [];
  }
  

  AddApp(name_app, link_app) {

    return <Application linkApp={link_app} nameApp={name_app} />
  }


  AppAutorized(app) {
    for (var i = 0; i < app.access.length; i++)
    {
      var elm = app.access[i];

      if (elm == this.access)
      {
        return true;
      }
    }
    return false;
  }

  CanAcess() {
    var AccessRight = [];

    for (var i = 0; i < this.AllApps.length; i++)
    {
      var elm = this.AllApps[i];

      if (this.AppAutorized(elm))
      {
        AccessRight.push(elm);
      }
    }
    return AccessRight;
  }

  render() {
    this.AllApps.push({name: "test", link_: "https://youtu.be/dQw4w9WgXcQ?autoplay=1", access: ['root','1','a'],})

    this.AllApps.push({name: "l'autre teste", link_: "https://youtu.be/Cc7oi_D4WtI", access: ['root','2','a'],})
    
    const accessright = this.CanAcess();

    const item = [];

    for (const [index, value] of accessright.entries()) {
      item.push(<p key={index}>{this.AddApp(value.name, value.link_)}</p>)
    }

    return (
      <div>
        {item}
      </div>
    );
  }
}




class Application extends React.Component {
  constructor(props) {
    super(props);

      this.linkAp = props.linkApp;
      
      this.nameAp = props.nameApp;
  }



    render() {
      
      return (
        <a href={this.linkAp} >
          <button>
            {this.nameAp}
          </button>
        </a>
      );
    }
}

ReactDOM.render(
  <Rights />,
  //<Board />,
  document.getElementById('root')
);